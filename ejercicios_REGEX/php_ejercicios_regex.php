<?php

//Realizar una expresión regular que detecte emails correctos.
$regexEMAIL = '/[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+/m';
echo"<h3>REGEX para email</h3>";
echo"<h4>{$regexEMAIL}</h4>";
$check1 ="hola@gmail.com";
$check2 ="@gmail.com";

$result = preg_match($regexEMAIL, $check1);
$result2 = preg_match($regexEMAIL, $check2);

echo "El resultado para \"{$check1}\" es ".$result;
echo "<br>";
echo "El resultado para \"{$check2}\" es ".$result2;

//Realizar una expresion regular que detecte Curps Correctos
//ABCD123456EFGHIJ78.

echo"<h3>REGEX para CURP</h3>";
$regexCURP = '/^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/';
echo"<h4>{$regexCURP}</h4>";
//Mi curp
$check3 = "CAPO000407HMCBRSA1";
//Cambio MC (clave del EDOMEX) por SC 
$check4 = "CAPO000407HSCBRSA0";
//Un caracter màs corto
$check5 = "CAPO000407HMCBRSA";

$result3 = preg_match($regexCURP, $check3);
$result4 = preg_match($regexCURP, $check4);
$result5 = preg_match($regexCURP, $check5);

echo "El resultado para \"{$check3}\" es ".$result3;
echo "<br>";
echo "El resultado para \"{$check4}\" es ".$result4;
echo "<br>";
echo "El resultado para \"{$check5}\" es ".$result5;


//Realizar una expresion regular que detecte palabras de longitud mayor a 50
//formadas solo por letras.

echo"<h3>REGEX para palabras de más de 50 letras</h3>";
$regex50 = '/^[A-Za-z]{50,}/';
echo"<h4>{$regex50}</h4>";
//50 letras
$check6 = "sssssssssssssssssssssssddddddddeeeeeeeeeeeeeeessss";
//50 letras y un numero
$check7 = "sssssssssssssssssssssssddddddddeeeeeeeeeeeeeeesss2";
//49 letras 
$check8 = "sssssssssssssssssssssssddddddddeeeeeeeeeeeeeeesss";

$result6 = preg_match($regex50, $check6);
$result7 = preg_match($regex50, $check7);
$result8 = preg_match($regex50, $check8);

echo "El resultado para \"{$check6}\" es ".$result6;
echo "<br>";
echo "El resultado para \"{$check7}\" es ".$result7;
echo "<br>";
echo "El resultado para \"{$check8}\" es ".$result8;



//Crea una funci{on para escapar los simbolos especial
function escapar($cadena){
    //podemos agregar los caracteres que queremos escapar
    $chars = array("'", "\"" ,"*",".","!");
    $escapado ="";
    for($i=0; $i<strlen($cadena);$i++){
        if(in_array($cadena[$i],$chars)){
            continue;
        }else{
            $escapado = $escapado.$cadena[$i];
        }
    }
    return $escapado;
}

echo <<<_END
    <h3>Login</h3>
    <form action="php_ejercicios_regex.php" method="post">
        <div class="form-group row">
            <label for="inputCuenta3" class="col-sm-2 col-form-label">Cadena</label>
            <div class="col-sm-10">
                <input name="cadena" type="text" class="form-control" id="inputCuenta3" placeholder=Cadena">
            </div>
        <div class="form-group row">
        <div class="row">
            <div class="col">
            <input type="submit" value="Escapar">
            </div>
        </div>
    </form>
_END;



if($_POST){
    $info = $_POST["cadena"];
    echo "<br>Palabra sin escapar: {$info}<br>";
    echo "<br>Palabra escapada: ";
    echo (escapar($info));
    echo "<br>";
}
//Crear una expresion regular para detectar números decimales.

echo"<h3>REGEX para números decimales</h3>";

$regexDECI = '/^\d+\.\d{0,2}$/';
echo"<h4>{$regexDECI}</h4>";
//50 letras
$check8 = "1";
//50 letras y un numero
$check9 = "1.2";

$result8 = preg_match($regexDECI, $check8);
$result9 = preg_match($regexDECI, $check9);


echo "El resultado para \"{$check8}\" es ".$result8;
echo "<br>";
echo "El resultado para \"{$check9}\" es ".$result9;



?>
