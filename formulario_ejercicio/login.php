<?php
echo <<<_END
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Login</title>
</head>
<body>
  
    <br>

    <h3>Login</h3>
    <form action="login.php" method="post">
        <div class="form-group row">
          <label for="inputCuenta3" class="col-sm-2 col-form-label">Número de Cuenta</label>
          <div class="col-sm-10">
            <input name="inputCuenta3" type="number" class="form-control" id="inputCuenta3" placeholder="Número de cuenta">
          </div>
        </div>
        <div class="form-group row">
          <label for="inputcontra3" class="col-sm-2 col-form-label">Password</label>
          <div class="col-sm-10">
            <input name="inputcontra3" type="password" class="form-control" id="inputcontra3" placeholder="Contraseña">
          </div>
        </div>
            <input type="submit" value="Iniciar sesión">
         
      </form>

      <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
_END;

//inicio la sesión
session_start();

//cada vez que se entre a login, se vacía la sesión "usuario"
unset($_SESSION["usuario"]);

//Se ejecuta esta parte del código cuando se envié la informulación del formulario
if ($_POST)
{

   
    //se precarga el usuario Admin
    $_SESSION['Alumno'][1]['num_cta'] = 1;
    $_SESSION['Alumno'][1]['nombre'] = 'Admin';
    $_SESSION['Alumno'][1]['primer_apellido'] = 'General';
    $_SESSION['Alumno'][1]['segundo_apellido'] = '';
    $_SESSION['Alumno'][1]['contrasena'] = 'adminpass123';
    $_SESSION['Alumno'][1]['genero'] = 'O';
    $_SESSION['Alumno'][1]['fecha_nac'] = '25/01/1990';

    //Se compara las contraseñas
    if ($_SESSION['Alumno'][ $_POST['inputCuenta3'] ]['contrasena'] == $_POST['inputcontra3']){
        
        //Se guarda que usuario ha ingresado 
        $_SESSION['usuario']=$_SESSION['Alumno'][$_POST['inputCuenta3']]['num_cta'];
        //Se redirecciona a la página info
        header("Location: info.php");

       
    }else{ 
        //Se envia la alerta de contraseña incorrecta
        echo <<<_END
            <div class="alert alert-danger" role="alert">
                Contraseña incorrecta
            </div>
        _END;
    }
}

?>