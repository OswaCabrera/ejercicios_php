<?php

//Se inicia la session
session_start();

//Si no se ha autenticado ni un usuario se redirecciona a login.php 
if(empty($_SESSION["usuario"])){
    header("Location: login.php");
}else{
    //código de html
    echo <<<_END
    <html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>info</title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <style>
        .row{
            padding-top: .75rem;
            padding-bottom: .75rem;
            backgound-color: rgba(39,41,43,0.03);
            border: 1px solid rgba(39,41,43,0.1);
        }

    </style>

    </head>
    <body>
        <ul class="nav nav-pills">
            <li class="nav-item">
                <a class="nav-link active" href="info.php">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="formulario.php">Registrar Alumnos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="login.php">Cerrar Sesión</a>
            </li>
        </ul>

        <main>
        
        <br>
        
        <div class="container">
        <h3>Usuario Autenticado</h3>
            <div class="row">
                <div class="col">
                    {$_SESSION['Alumno'][ $_SESSION['usuario'] ]['nombre']}
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h5>Información</h5>
                    Número de cuenta: {$_SESSION["Alumno"][$_SESSION['usuario']]["num_cta"]}
                    <br><br>
                    Fecha de nacimiento: {$_SESSION["Alumno"][$_SESSION['usuario']]["fecha_nac"]}
                </div>
            </div>
            <br><br>
            <h2>Datos guardados</h2>
            <div class="row">
                <div class="col">#</div>
                <div class="col">Nombre</div>
                <div class="col">Fecha de Nacimiento</div>
            </div>
        </div>

        
        </main>
    </body>
    </html>
    _END;

    //ciclo para recorrer el arreglo de $_SESSION
    foreach($_SESSION as $val1){
        foreach($val1 as $val2){
            //Se le da formato a la salida
            echo <<<_END
            <div class="container">
                <div class="row">
                    <div class="col">
                        {$val2["num_cta"]}
                    </div>
                    <div class="col">
                        {$val2["nombre"]}
                    </div>
                    <div class="col">
                        {$val2["fecha_nac"]}
                    </div>
                </div>
            </div>
    
            _END;
        }

    }
    

}

?>