<?php

//Iniciamos la session
session_start();

//Si no se ha autenticado ni un usuario se redirecciona a login.php 
if(empty($_SESSION["usuario"])){
    header("Location: login.php");
}else{
    echo <<<_END
    <html lang="es">
    <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Formulario</title>
    </head>
    <body>

    <ul class="nav nav-pills">
        <li class="nav-item">
            <a class="nav-link" href="info.php">Home</a>
        </li>
        <li class="nav-item">
            <a class="nav-link active" href="formulario.php">Registrar Alumnos</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="login.php">Cerrar Sesión</a>
        </li>
    </ul>

    <br>
        <form action="formulario.php" method="post">
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Número de cuenta</label>
            <div class="col-sm-10">
            <input name="num_cuenta" type="number" class="form-control" id="inputEmail3" placeholder="Número de cuenta">
            </div>
        </div>
        <div class="form-group row">
            <label for="inputName3" class="col-sm-2 col-form-label">Nombre</label>
            <div class="col-sm-10">
                <input name="nombre" type="text" class="form-control" id="inputName3" placeholder="Nombre" required>
            </div>
        </div>
        <div class="form-group row">
            <label for="input1ape3" class="col-sm-2 col-form-label">Primer Apellido</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="input1ape3" name="ape1" placeholder="Primer Apellido" required>
            </div>
        </div>
        <div class="form-group row">
            <label for="input2ape3" class="col-sm-2 col-form-label">Segundo Apellido</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="input2ape3" name="ape2" placeholder="Segundo Apellido" required>
            </div>
        </div>
        <fieldset class="form-group">
            <div class="row">
            <legend class="col-form-label col-sm-2 pt-0">Genero</legend>
            <div class="col-sm-10">
                <div class="form-check">
                <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="H" checked>
                <label class="form-check-label" for="gridRadios1">
                    Hombre
                </label>
                </div>
                <div class="form-check">
                <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios2" value="M">
                <label class="form-check-label" for="gridRadios2">
                    Mujer
                </label>
                </div>
                <div class="form-check disabled">
                <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios3" value="O" >
                <label class="form-check-label" for="gridRadios3">
                    Otro
                </label>
                </div>
            </div>
            </div>
        </fieldset>
        <div class="form-group row">
            <label for="inpudate3" class="col-sm-2 col-form-label">Fecha de nacimiento</label>
            <div class="col-sm-10">
                <input name="fecha" type="date" class="form-control" id="inputdate3" placeholder="dd/mm/aaaa" required>
            </div>
        </div>
        <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
            <div class="col-sm-10">
                <input name="contrasena" type="password" class="form-control" id="inputPassword3" placeholder="Password" required>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-10">
            <button type="submit" class="btn btn-primary">Resgistrar</button>
            </div>
        </div>
        </form>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </body>
    </html>
    _END;

    //Cuando se envie el formulario lo guardamos en su respectiva session
    if($_POST){
        $_SESSION['Alumno'][$_POST["num_cuenta"]]['num_cta'] = $_POST["num_cuenta"];
        $_SESSION['Alumno'][$_POST['num_cuenta']]['nombre'] = $_POST["nombre"];
        $_SESSION['Alumno'][$_POST["num_cuenta"]]['primer_apellido'] = $_POST["ape1"];
        $_SESSION['Alumno'][$_POST["num_cuenta"]]['segundo_apellido'] = $_POST["ape2"];
        $_SESSION['Alumno'][$_POST["num_cuenta"]]['contrasena'] = $_POST["contrasena"];
        $_SESSION['Alumno'][$_POST["num_cuenta"]]['genero'] = $_POST["gridRadios"];
        $_SESSION['Alumno'][$_POST["num_cuenta"]]['fecha_nac'] = $_POST["fecha"] ;

    }

}



?>